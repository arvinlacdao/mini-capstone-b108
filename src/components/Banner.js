import React from 'react';
import { Jumbotron, Button } from 'react-bootstrap';

export default function Banner() {
	return (
		<Jumbotron>
		    <h1>Zuitt Store</h1>
		    <p>Products for everyone, everywhere.</p>
		    <Button variant="primary">Purchase now!</Button>
		</Jumbotron>
	)
}