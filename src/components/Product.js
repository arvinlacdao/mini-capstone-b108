import React from 'react';
import { Card, Button, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Product({product}) {

    const {name, description, price} = product;

    return (
        <Col lg={3} md={4} xs={12} className="my-3">
            <Card className="productCard">
                <Card.Img variant="top" src="https://place-puppy.com/300x300" />
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Link to={`/viewproduct?productId=${product._id}`}>
                        <Button variant="info">View Product</Button>
                    </Link>
                    <Card.Subtitle className="mt-3">Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )
}