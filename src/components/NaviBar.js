import React, { useContext } from 'react';
// Import the necessary components from react-bootstrap
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function NaviBar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
			<Navbar.Brand href="/" className="mx-5">Zuitt Store</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" className="mx-3"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link href="/">Home</Nav.Link>
					<Nav.Link href="/products">Products</Nav.Link>
					{(user.id !== null) ? 
						<Nav.Link href="/logout">Logout</Nav.Link>
						:
						<React.Fragment>
							<Nav.Link href="/login">Login</Nav.Link>
							<Nav.Link href="/register">Register</Nav.Link>
						</React.Fragment>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}