import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import NaviBar from './components/NaviBar';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import Register from './pages/Register';
import ViewProduct from './pages/ViewProduct';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	})

	// Function for clearing the localStorage upon logout and setting the user state to it's original value
	const unsetUser = () => {

	    localStorage.clear();

	    setUser({
	        id: null,
	        isAdmin: null
	    });

	}

	useEffect(() => {
	    fetch(`https://sheltered-savannah-50416.herokuapp.com/api/users/details`, {
	        headers: { Authorization: `Bearer ${ localStorage.getItem('token') }` }
	    })
	    .then(res => res.json())
	    .then((data) => {
	        if (data._id !== undefined) {
	            setUser({ id: data._id, isAdmin: data.isAdmin })
	        } else {
	            setUser({ id: null, isAdmin: null })   
	        }
	    })
	}, [user.id])

	return (
		<React.Fragment>
			<UserProvider value={{user, setUser, unsetUser}}>
				<Router>
					<NaviBar />
					<Container className="my-5">
						<Switch>
							<Route exact path="/" component={Home}/>
							<Route exact path="/login" component={Login}/>
							<Route exact path="/logout" component={Logout}/>
							<Route exact path="/products" component={Products}/>
							<Route exact path="/register" component={Register}/>
							<Route exact path="/viewproduct" component={ViewProduct}/>
						</Switch>
					</Container>
				</Router>
			</UserProvider>
		</React.Fragment>
	);
}

export default App;