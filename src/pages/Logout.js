import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {

    // Consume the UserContext and destructure it to access the unsetUser function from the context provider
    const { unsetUser } = useContext(UserContext);
    
    // Invoke unsetUser only after initial render
    useEffect(() => {
        // Invoke unsetUser() to clear local storage of user info
        unsetUser();
    })

    return(
    	<Redirect to="/login"/>
    );

}