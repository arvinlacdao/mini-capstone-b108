import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';

export default function Register() {

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState(0);
	const [password, setPassword] = useState("");
	const [verifyPassword, setVerifyPassword] = useState("");
	// State for form validation
	const [isActive, setIsActive] = useState(false);
	// State for page redirection
	const [isRedirecting, setIsRedirecting] = useState(false);

	function registerUser(e){

		e.preventDefault();

		fetch("https://sheltered-savannah-50416.herokuapp.com/api/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true){

				Swal.fire("Duplicate email exists", "Email is already taken. Kindly use a different email address.", "error");

			} else {

				fetch("https://sheltered-savannah-50416.herokuapp.com/api/users", {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstname: firstName,
						lastname: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					
					if (data === true){
						Swal.fire("Registration Successful", "Thank you for registering!", "success");
						setIsRedirecting(true);
					} else {
						Swal.fire("Something went wrong", "Your registration may not be completed for now. Try again later.", "success");
					}

				})

			}
		})
		
	}

	// Validation to enable submit button when all fields are populated and both passwords match
	useEffect(() => {

	    if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }

	}, [firstName, lastName, email, mobileNo, password, verifyPassword]);

	return (
		<React.Fragment>
			{ isRedirecting ? 
					<Redirect to='/login' /> 
				: 
					<Form onSubmit={e => registerUser(e)}>
						<Form.Group controlId="firstName">
						    <Form.Label>First Name</Form.Label>
						    <Form.Control 
						        type="text" 
						        placeholder="Enter first name"
						        value={firstName}
						        onChange={e => setFirstName(e.target.value)}
						        required
						        autoComplete="true"
						    />
						</Form.Group>

						<Form.Group controlId="lastName">
						    <Form.Label>Last Name</Form.Label>
						    <Form.Control 
						        type="text" 
						        placeholder="Enter last name"
						        value={lastName}
						        onChange={e => setLastName(e.target.value)}
						        required
						        autoComplete="true"
						    />
						</Form.Group>

					    <Form.Group controlId="userEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					            type="email" 
					            placeholder="Enter email"
					            value={email}
						        onChange={e => setEmail(e.target.value)}
					            required
					            autoComplete="true"
					        />
					        <Form.Text className="text-muted">
					            We'll never share your email with anyone else.
					        </Form.Text>
					    </Form.Group>

					    <Form.Group controlId="mobileNo">
					        <Form.Label>Mobile Number</Form.Label>
					        <Form.Control 
					            type="number" 
					            placeholder="Enter mobile number"
					            value={mobileNo}
						        onChange={e => setMobileNo(e.target.value)}
					            required
					            autoComplete="true"
					        />
					    </Form.Group>

					    <Form.Group controlId="password1">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					            type="password" 
					            placeholder="Password"
					            value={password}
						        onChange={e => setPassword(e.target.value)}
					            required
					            autoComplete="true"
					        />
					    </Form.Group>

					    <Form.Group controlId="password2">
					        <Form.Label>Verify Password</Form.Label>
					        <Form.Control 
					            type="password" 
					            placeholder="Verify Password"
					            value={verifyPassword}
						        onChange={e => setVerifyPassword(e.target.value)}
					            required
					            autoComplete="true"
					        />
					    </Form.Group>

					    {isActive ? 
							    <Button variant="primary" type="submit" id="submitBtn" className="mt-3">
						            Submit
						        </Button>
					        :
					    	    <Button variant="danger" type="submit" id="submitBtn" className="mt-3">
					                Submit
					            </Button>
					    }

					</Form>
			}
		</React.Fragment>
		
	)
}