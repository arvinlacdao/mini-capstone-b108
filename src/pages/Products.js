import React, { useState, useEffect } from 'react';
import { Row } from 'react-bootstrap';
import Product from '../components/Product';

export default function Products() {

	const [products, setProducts] = useState([])

	useEffect(() => {

		fetch("https://sheltered-savannah-50416.herokuapp.com/api/items")
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map(product => {
				return <Product key={product._id} product={product}/>
			}))

		})

	}, [])

	return (
		<React.Fragment>
			<Row>
				{products}
			</Row>
		</React.Fragment>
	)
}