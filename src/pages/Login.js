import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Login() {

	const { setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	// State for form validation
	const [isActive, setIsActive] = useState(false);
	// State for page redirection
	const [isRedirecting, setIsRedirecting] = useState(false);

	function authenticate(e) {

		e.preventDefault();

		fetch("https://sheltered-savannah-50416.herokuapp.com/api/users/login", {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			
			if (data === false){
				Swal.fire("Invalid Credentials", "Please input the correct username and/or password", "error");
			} else {

				localStorage.setItem('token', data.access)

				fetch("https://sheltered-savannah-50416.herokuapp.com/api/users/details", {
					headers: { Authorization: `Bearer ${data.access}`}
				})
				.then(res => res.json())
				.then(data => {

					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					});

				})

				setIsRedirecting(true);

			}

		})

	}

	// Validation to enable submit button when all fields are populated and both passwords match
	useEffect(() => {

	    if( email !== '' && password !== '' ){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }

	}, [email, password]);

	return (
		<React.Fragment>
			{ isRedirecting ?
					<Redirect to="/products"/>
				:
					<Form onSubmit={e => authenticate(e)}>
						<Form.Group controlId="email">
						    <Form.Label>Email Address</Form.Label>
						    <Form.Control 
						        type="text" 
						        placeholder="Email"
			                    value={email}
			        	        onChange={e => setEmail(e.target.value)}
						        required
						        autoComplete="true"
						    />
						</Form.Group>

					    <Form.Group controlId="password">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					            type="password" 
					            placeholder="Password"
			                    value={password}
			        	        onChange={e => setPassword(e.target.value)}
					            required
					            autoComplete="true"
					        />
					    </Form.Group>

			    	    {isActive ? 
			    			    <Button variant="primary" type="submit" id="submitBtn" className="mt-3">
			    		            Login
			    		        </Button>
			    	        :
			            	    <Button variant="danger" type="submit" id="submitBtn" className="mt-3">
			                        Login
			                    </Button>
			            }

					</Form>
			}
		</React.Fragment>
	)
}