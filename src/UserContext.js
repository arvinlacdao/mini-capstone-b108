import React from 'react';

// Creates a Context object
const UserContext = React.createContext();

// Provider component that allows other components to use context object changes/data
export const UserProvider = UserContext.Provider;

export default UserContext;